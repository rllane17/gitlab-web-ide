export const getBaseUrlFromLocation = () => {
  const newUrl = new URL('web-ide/public', window.location.href);

  return newUrl.href;
};

export const getRootUrlFromLocation = () => {
  const newUrl = new URL('/', window.location.href);

  return newUrl.href;
};
