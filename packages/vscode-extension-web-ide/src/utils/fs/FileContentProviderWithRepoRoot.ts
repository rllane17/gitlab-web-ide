import type { IFileContentProvider } from '@gitlab/web-ide-fs';
import { stripPathRoot } from '../stripPathRoot';

/**
 * Decorator for IFileContentProvider that strips repo root from calls to getContent
 */
export class FileContentProviderWithRepoRoot implements IFileContentProvider {
  private readonly _base: IFileContentProvider;

  private readonly _repoRoot: string;

  constructor(base: IFileContentProvider, repoRoot: string) {
    this._base = base;
    this._repoRoot = repoRoot;
  }

  getContent(path: string): Promise<Uint8Array> {
    return this._base.getContent(stripPathRoot(path, this._repoRoot));
  }
}
