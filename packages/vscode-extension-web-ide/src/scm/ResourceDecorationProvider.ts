import * as vscode from 'vscode';
import { toFileDecoration } from './status';
import { IStatusViewModel } from './types';

export class ResourceDecorationProvider {
  private readonly _decorationsDidChange: vscode.EventEmitter<vscode.Uri[]>;

  private readonly _fileDecorations: Map<string, vscode.FileDecoration>;

  constructor() {
    this._decorationsDidChange = new vscode.EventEmitter();

    this._fileDecorations = new Map();
  }

  update(statusVms: IStatusViewModel[]) {
    this._fileDecorations.clear();
    statusVms.forEach(statusVm => {
      this._fileDecorations.set(statusVm.uri.toString(), toFileDecoration(statusVm));
    });

    this._decorationsDidChange.fire(statusVms.map(x => x.uri));
  }

  createVSCodeDecorationProvider(): vscode.FileDecorationProvider {
    return {
      onDidChangeFileDecorations: this._decorationsDidChange.event,
      provideFileDecoration: (uri: vscode.Uri) => this._fileDecorations.get(uri.toString()),
    };
  }
}
