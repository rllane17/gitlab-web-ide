import * as vscode from 'vscode';
import { GitLabProject } from '@gitlab/vscode-mediator-commands';
import * as mediator from '../../mediator';
import { RELOAD_COMMAND_ID } from '../../constants';
import { generateCommitMessage } from './generateCommitMessage';
import { promptBranchName } from './promptBranchName';
import { getCommitPayload } from './getCommitPayload';
import { showSuccessMessage } from './showSuccessMessage';
import { showCommitErrorMessage } from './showCommitErrorMessage';
import { IReadonlySourceControlViewModel } from '../types';

interface CommandFactoryOptions {
  readonly branchName: string;
  readonly branchMergeRequestUrl: string;
  readonly commitId: string;
  readonly viewModel: IReadonlySourceControlViewModel;
  readonly project: GitLabProject;
}

/**
 * Factory for the "commit" command
 *
 * It's important the command itself doesn't take any arguments so that
 * it can be triggered by the user.
 */
export const factory =
  ({
    branchName: defaultBranchName,
    branchMergeRequestUrl,
    commitId: startingSha,
    viewModel,
    project,
  }: CommandFactoryOptions) =>
  async (): Promise<void> => {
    const status = viewModel.getStatus();

    if (!status.length) {
      await vscode.window.showInformationMessage('No changes found. Not able to commit.');
      return;
    }

    const commitMessage = viewModel.getCommitMessage() || generateCommitMessage(status);

    const branchSelection = project.empty_repo
      ? // Even though `empty_repo` implies this is technically a new branch, the GitLab API won't expect us to treat it this way
        { isNewBranch: false, branchName: defaultBranchName }
      : await promptBranchName(defaultBranchName);

    // User canceled the operation
    if (!branchSelection) {
      return;
    }

    const { branchName, isNewBranch } = branchSelection;

    try {
      await mediator.commit(
        getCommitPayload({
          status,
          commitMessage,
          branchName,
          isNewBranch,
          startingSha,
        }),
      );
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
      await showCommitErrorMessage(e);
      return;
    }

    // If we created from a new branch, don't use the MR URL
    const mrUrl = isNewBranch ? '' : branchMergeRequestUrl;

    // We just want to fire and forget here
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    showSuccessMessage({ project, branchName, mrUrl });

    await vscode.commands.executeCommand(RELOAD_COMMAND_ID, { ref: branchName });
  };
