import * as vscode from 'vscode';
import { GitLabCodeCompletionProvider } from './GitlabCodeCompletionProvider';
import { log, initializeLogging } from '../log';

jest.mock('../log');

describe('GitLabCodeCompletionProvider', () => {
  const mockPrompt = 'const areaOfCube = ';
  const mockEmptyDocument: Partial<vscode.TextDocument> = {
    getText: () => '',
    lineAt: () => ({ text: '' } as vscode.TextLine),
  };
  const mockDocument: Partial<vscode.TextDocument> = {
    getText: () => mockPrompt,
    lineAt: () => ({ text: mockPrompt } as vscode.TextLine),
  };
  const mockPosition = {
    line: 0,
    character: mockPrompt.length,
  } as vscode.Position;
  const expectedServer = 'https://codesuggestions.gitlab.com/v1/completions';
  const choice = '(side) => ';
  const mockCompletions = { choices: [{ text: choice }] };
  const mockInlineCompletions = [] as vscode.InlineCompletionItem[];

  describe('fetchCompletions', () => {
    const mockFetchResponse = {
      ok: true,
      status: 200,
      json: () => Promise.resolve(mockCompletions),
    } as Response;
    const mockFetchFailureResponse = {
      ok: false,
      status: 401,
      json: () => Promise.resolve(mockCompletions),
      text: () => Promise.resolve(choice),
      url: 'http://example.org',
    } as Response;
    const fetchParams = {
      model: 'gitlab',
      prompt: mockPrompt,
      stop: [],
    };

    it('calls fetch and returns data', async () => {
      global.fetch = jest.fn().mockResolvedValue(mockFetchResponse);

      const glcp = new GitLabCodeCompletionProvider();
      const data = await glcp.fetchCompletions(fetchParams);
      expect(fetch).toHaveBeenCalledWith(expectedServer, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(fetchParams),
      });
      expect(data).toEqual(mockCompletions);
    });

    it('when fetch failures occur', async () => {
      global.fetch = jest.fn().mockResolvedValue(mockFetchFailureResponse);
      global.console.warn = jest.fn();

      const glcp = new GitLabCodeCompletionProvider();
      const data = await glcp.fetchCompletions(fetchParams);
      expect(fetch).toHaveBeenCalledWith(expectedServer, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(fetchParams),
      });
      expect(data).toEqual([]);
    });

    it('logs a fetch error', async () => {
      initializeLogging(() => jest.fn());
      global.fetch = jest.fn().mockResolvedValue(mockFetchFailureResponse);
      log.warn = jest.fn();

      const glcp = new GitLabCodeCompletionProvider();
      await glcp.fetchCompletions(fetchParams);
      expect(log.warn).toHaveBeenCalledWith(
        `Fetching code suggestions from http://example.org failed for server https://codesuggestions.gitlab.com/v1/completions. Body: ${choice}`,
      );
    });
  });

  describe('getCompletions', () => {
    it('when no prompt returns empty array', async () => {
      const glcp = new GitLabCodeCompletionProvider();
      glcp.fetchCompletions = jest.fn().mockResolvedValue(mockCompletions);

      const inlineCompletions = await glcp.getCompletions(
        mockEmptyDocument as unknown as vscode.TextDocument,
        mockPosition,
      );

      expect(inlineCompletions).toEqual([]);
    });

    it('with a prompt, returns completions', async () => {
      const glcp = new GitLabCodeCompletionProvider();
      glcp.fetchCompletions = jest.fn().mockResolvedValue(mockCompletions);

      const inlineCompletions = await glcp.getCompletions(
        mockDocument as unknown as vscode.TextDocument,
        mockPosition,
      );

      expect(inlineCompletions[0].insertText).toEqual('(side) => ');
    });
  });

  describe('provideInlineCompletionItems', () => {
    const mockContext = {
      triggerKind: vscode.InlineCompletionTriggerKind.Automatic,
    } as vscode.InlineCompletionContext;
    jest.useFakeTimers();

    // Jest timers with multiple async resolvers https://github.com/facebook/jest/issues/11876
    it('provides inline completions', async () => {
      const glcp: GitLabCodeCompletionProvider = new GitLabCodeCompletionProvider(true);
      glcp.getCompletions = jest.fn().mockResolvedValue(mockInlineCompletions);

      jest.runAllTimers();
      await glcp.provideInlineCompletionItems(
        mockDocument as unknown as vscode.TextDocument,
        mockPosition,
        mockContext,
      );
      jest.runAllTimers();

      expect(glcp.getCompletions).toHaveBeenCalled();
      jest.runAllTimers();
    });
  });
});
