import * as vscode from 'vscode';
import { AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE } from '../constants';

export const disableCodeSuggestions = async () => {
  await vscode.workspace
    .getConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE)
    .update('enabled', false, vscode.ConfigurationTarget.Global);
};

export const enableCodeSuggestions = async () => {
  await vscode.workspace
    .getConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE)
    .update('enabled', true, vscode.ConfigurationTarget.Global);
};
