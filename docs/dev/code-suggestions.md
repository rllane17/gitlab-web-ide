# AI-assisted Code Suggestions

With a local proxy like [`code-suggestions-proxy`](https://gitlab.com/a_akgun/code-suggestions-proxy), you can:

- Authenticate with your GitLab personal access token.
- Avoid any CORS issues.
- See log outputs for requests.

## Install a local proxy to Code Suggestions

To install the Code Suggestions proxy:

1. Clone the project and install npm packages:

```sh
git clone https://gitlab.com/a_akgun/code-suggestions-proxy.git
cd code-suggestions-proxy
npm install
# edit the .env file and add `CS_API=<YOUR PERSONAL ACCESS TOKEN>`
```

1. In `constant.ts`, modify `AI_ASSISTED_CODE_SUGGESTIONS_API_URL`:

```js
-export const AI_ASSISTED_CODE_SUGGESTIONS_API_URL = 'https://codesuggestions.gitlab.com/v1/completions';
+export const AI_ASSISTED_CODE_SUGGESTIONS_API_URL = 'http://127.0.0.1:8037/v1/completions';
```

1. Run the Code Suggestions proxy:

```sh
npm run start
```

## Start the Web IDE

To start the Web IDE, in the `gitlab-web-ide` project root directory, run this command:

```sh
yarn start:example
```

## Avoid caching

To avoid caching, use the Chrome Incognito mode by closing and opening Chrome each time you make changes.
